### Canonical Landscape (personal/on-prem)

Ansible configuration for running Landscape on-prem.

## Prerequisites

* Ansible installed locally
* Ubuntu 18.04 server or VM (20.04 not supported at time of writing)
* Ubuntu client machine(s)
* Local DNS entry for `landscape.local` or your chosen hostname (or a modified `/etc/hosts` file on the client)

## Ansible configuration

Ensure that you modify your `/etc/ansible/hosts` file with your desired server's IP with something like the below:
```
[landscape]
landscape01 ansible_host=10.0.0.1

```

Create an `ansible` user on your server and allocate passwordless sudo permissions. To do this, add a new user (`sudo adduser ansible && sudo adduser ansible sudo`) and edit `/etc/sudoers` by running `sudo visudo` and add the following line at the bottom:
```
ansible	ALL=(ALL:ALL) NOPASSWD:ALL
```

Make sure to copy your ssh key into `/home/ansible/.ssh/authorized_keys` and that the file & parent directories have the correct permissions.

# Provisioning

```
ansible-playbook landscape.yml
```

## Client configuration

The quickstart package generates and installs a self-signed SSL certificate in /etc/ssl/certs/landscape_server_ca.crt using the FQDN of the host for the commonName field of the certificate. A copy of this file will be needed on each computer that you register with LDS.

On each computer, copy that certificate over to, say, /etc/landscape/server.pem and add this line to the configuration file /etc/landscape/client.conf:
```
    ssl_public_key = /etc/landscape/server.pem
```

Then run this following on the client:
```
sudo landscape-config --computer-title "My Computer Name" --account-name standalone  -p MyRegistrationKey --url https://landscape.local/message-system --ping-url http://landscape.local/ping
```
Of course replacing `"My Computer Name"` and `MyRegistrationKey` with appropriate values.

# Potential issues

If using a `.local` domain, (m)DNS can be an issue, depending on your network, when registering clients. Make sure that, in `/etc/nsswitch.conf`, `dns` comes before `[NOTFOUND=return]` for the `hosts:` line, as per the below:
```
hosts:		files mdns4_minimal dns [NOTFOUND=return]
```
Also, if this doesn't work, try running the following which seems to work for Ubuntu 20.04 servers:
```
sudo ln -sf /run/systemd/resolve/resolv.conf /etc/resolv.conf
```